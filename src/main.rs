///anyhow pour le main et fichier (c'est trop bien merci super prof)
/// fmt pour Display
use anyhow::anyhow;
use std::fmt;

#[derive(Debug, PartialEq)]

/// # Instruction
// Crée une émunération Instruction pour définir le mouvement que doit suivre le/les robot(s)
pub enum Instruction {
    Right,
    Left,
    Forward,
    Nothing,
}

/// Orientation donne les orientations possible du robot
pub enum Orientation {
    W,
    E,
    S,
    N,
}

/// ajout d'une trait  Display pour les orientations durant l'affichage des robots
impl fmt::Display for Orientation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Orientation::W => write!(f, "Ouest"),
            Orientation::E => write!(f, "Est"),
            Orientation::S => write!(f, "Sud"),
            Orientation::N => write!(f, "Nord"),
        }
    }
}
/// la structure Robot contient tout les infos nécessaire à la création d'un robot et à son fonctionnement
struct Robot {
    id: i32,
    x: i32,
    y: i32,
    ori: Orientation,
    instr_list: Vec<String>,
}

///impl de fonction pour faire fonctionner le robot
impl Robot {
    ///pose l'orientation du robot
    fn set_orientation(&mut self, given_ori: &str) {
        match given_ori {
            "W" => self.ori = Orientation::W,
            "E" => self.ori = Orientation::E,
            "N" => self.ori = Orientation::N,
            "S" => self.ori = Orientation::S,
            _ => self.ori = Orientation::N,
        }
    }

    ///turn fais que selon l'instruction la nouvelle orientation du robot sera donné

    fn turn(&mut self, moveto: Instruction) {
        match moveto {
            Instruction::Left => match self.ori {
                Orientation::W => {
                    self.ori = Orientation::S;
                }
                Orientation::E => {
                    self.ori = Orientation::N;
                }
                Orientation::N => {
                    self.ori = Orientation::W;
                }
                Orientation::S => {
                    self.ori = Orientation::E;
                }
            },

            Instruction::Right => match self.ori {
                Orientation::W => {
                    self.ori = Orientation::N;
                }
                Orientation::E => {
                    self.ori = Orientation::S;
                }
                Orientation::N => {
                    self.ori = Orientation::E;
                }
                Orientation::S => {
                    self.ori = Orientation::W;
                }
            },
            ///les 2 cas qui ne change
            Instruction::Forward => {}

            Instruction::Nothing => {}
        }
    }

    //check_move vérifie si l'instruction est faisable ou non dans le monde (utiliser dans make_move)
    fn check_move(&mut self, move_given: &Instruction, moundo: &Monde) -> bool {
        let check_point = ". ".to_string();
        ///si cela corresponds a check_point c'est que l'éxecution est possible sinon pas du tout
        match move_given {
            Instruction::Left => match &moundo.map[(self.x + (moundo.x * (self.y)) - 1) as usize] {
                check_point => true,
                _ => false,
            },
            Instruction::Right => {
                match &moundo.map[((self.x) + (moundo.x * (self.y)) + 1) as usize] {
                    check_point => true,
                    _ => false,
                }
            }
            Instruction::Forward => match self.ori {
                ///match de l'ori parce que l'orientation change la valeur a check avec Forward
                Orientation::S => {
                    match &moundo.map[((self.x) + (moundo.x * ((self.y) + 1))) as usize] {
                        check_point => true,
                        _ => false,
                    }
                }

                Orientation::N => {
                    match &moundo.map[((self.x) + (moundo.x * ((self.y) - 1))) as usize] {
                        check_point => true,
                        _ => false,
                    }
                }

                Orientation::E => {
                    match &moundo.map[((self.x) + (moundo.x * (self.y)) + 1) as usize] {
                        check_point => true,
                        _ => false,
                    }
                }
                Orientation::W => {
                    match &moundo.map[((self.x) + (moundo.x * (self.y)) - 1) as usize] {
                        check_point => true,
                        _ => false,
                    }
                }
            },
            Instruction::Nothing => false,
        }
    }

    ///éxecute le mouvement en swapant dans le monde si il est faisable
    fn make_move(&mut self, moundo: &mut Monde) {
        for rob in 0..self.instr_list.len() {
            let save_pose = (self.x, self.y);
            let move_given = read_char(&self.instr_list[rob]);
            let check_true = self.check_move(&move_given, &moundo);

            if check_true == true {
                match move_given {
                    Instruction::Left => {
                        self.turn(move_given);
                        moundo.map[(self.x + (moundo.x * self.y)) as usize] =
                            Arrow_Orientation(&self.ori);
                        self.x -= 1;
                        moundo.map.swap(
                            (save_pose.0 + (moundo.x * (save_pose.1))) as usize,
                            (self.x + (moundo.x * self.y)) as usize,
                        );
                    }
                    Instruction::Right => {
                        self.turn(move_given);
                        moundo.map[(self.x + (moundo.x * self.y)) as usize] =
                            Arrow_Orientation(&self.ori);
                        self.x += 1;
                        moundo.map.swap(
                            (save_pose.0 + (moundo.x * (save_pose.1))) as usize,
                            (self.x + (moundo.x * self.y)) as usize,
                        );
                    }
                    Instruction::Forward => match self.ori {
                        ///meme raison que pour check_move seulement que désormais on éxecuter le mouvement
                        Orientation::S => {
                            self.y += 1;
                            moundo.map.swap(
                                (save_pose.0 + (moundo.x * (save_pose.1))) as usize,
                                (self.x + (moundo.x * self.y)) as usize,
                            );
                        }
                        Orientation::N => {
                            self.y -= 1;
                            moundo.map.swap(
                                (save_pose.0 + (moundo.x * (save_pose.1))) as usize,
                                (self.x + (moundo.x * self.y)) as usize,
                            );
                        }
                        Orientation::E => {
                            self.x += 1;
                            moundo.map.swap(
                                (save_pose.0 + (moundo.x * (save_pose.1))) as usize,
                                (self.x + (moundo.x * self.y)) as usize,
                            );
                        }
                        Orientation::W => {
                            self.x -= 1;
                            moundo.map.swap(
                                (save_pose.0 + (moundo.x * (save_pose.1))) as usize,
                                (self.x + (moundo.x * self.y)) as usize,
                            );
                        }
                    },
                    Instruction::Nothing => {}
                }
            } else {
                println!(
                    "💥 Une collision va se produire soit avec un robot ou la bordure du monde"
                );
                println!("instruction regetée");
            }
        }
    }
}

/// Struct Monde ou vont se déplacer les robots
struct Monde {
    x: i32,
    y: i32,
    map: Vec<String>,
}

///impl de fonction pour monde
impl Monde {
    /// créée le monde
    fn create_world(&mut self) {
        let mut tmpmap = vec![". ".to_string(); ((self.x + 1) * (self.y + 1)) as usize];
        (*self).map.append(&mut tmpmap);
    }

    /// pose la position de base des robots
    fn set_robot_position(&mut self, robot: &Robot) {
        self.map[(robot.x + (self.x * robot.y)) as usize] = Arrow_Orientation(&robot.ori);
    }
    ///affiche le monde
    fn display_world(&self, Rob_vec: &Vec<Robot>) {
        let mut count_y = 1;
        println!("Terrain{{x_max={};y_max={}}}", self.x, self.y);
        println!("Robots [");
        for n in 0..Rob_vec.len() {
            println!(
                "{{ id = {}, x = {}; y = {}; orientation: {}, instruction:{:?} }}",
                Rob_vec[n].id,
                Rob_vec[n].x - 1,
                Rob_vec[n].y,
                Rob_vec[n].ori,
                Rob_vec[n].instr_list
            );
        }
        println!("]");

        for i in 0..self.map.len() {
            if (self.x + 1) * count_y == (i) as i32 {
                print!("\n");
                count_y += 1;
            }
            print!("{}", self.map[i]);
        }
        print!("\n");
    }
}

/// # Fonction readchar
///
/// Permet de lire le caractère saisi et de créer l'Instruction voulue
/// Prend en paramère un &String
/// Renvoie une Instruction

fn read_char(choice: &String) -> Instruction {
    ///pas très clean dsl
    let right = "R".to_string();
    let left = "L".to_string();
    let forward = "F".to_string();
    match choice {
        right => Instruction::Right,
        left => Instruction::Left,
        forward => Instruction::Forward,
        _ => Instruction::Nothing,
    }
}

///# Fonction Arrow_Orientation
/// selon l'orientation du robot
/// une flèche correspondante lui sera attribué sur la map

fn Arrow_Orientation(lookingAt: &Orientation) -> String {
    match lookingAt {
        Orientation::W => "← ".to_string(),
        Orientation::E => "→ ".to_string(),
        Orientation::N => "↑ ".to_string(),
        Orientation::S => "↓ ".to_string(),
    }
}


/// le main qui lit le fichier et sort les informations dedans (merci M.viala et son cadeau qui a beaucoup aidé)
/// créée le monde, pose les robot,les affiche et les fais bouger
fn main() -> anyhow::Result<()> {
    let buffer = std::fs::read_to_string("../two_robots.txt")?;
    let mut lines = buffer.lines();
    // x y
    let mut world = lines
        .next()
        .ok_or(anyhow!("Wtf no world"))?
        .split_whitespace();
    let x = world
        .next()
        .ok_or(anyhow!("Meh na x_max"))?
        .parse::<i32>()?;
    let y = world
        .next()
        .ok_or(anyhow!("Meh na y_max"))?
        .parse::<i32>()?;

    let mut i = 0;
    let mut engare: Vec<Robot> = Vec::new();

    // gestion des robots + Instructions
    // Pseudo code Créer vecteur robots
    while let (Some(mut robot), Some(mut instrs)) = (lines.next(), lines.next()) {
        ///si y a des ligne blanche
        while robot == "" {
            robot = lines.next().ok_or(anyhow!("damn boomer"))?;
            let tmp = robot;
            robot = instrs;
            instrs = tmp;
        }
        ///le robots qui sera push au engare quand il aura ses données
        let mut el_rob = Robot {
            id: i,
            x: 0,
            y: 0,
            ori: Orientation::N,
            instr_list: Vec::new(),
        };
        ///split des données
        let mut LesRobot = robot.split_whitespace();
        let mut LesInstrs = instrs.split("");

        //récupération des données
        el_rob.x = LesRobot.next().ok_or(anyhow!("pb x"))?.parse::<i32>()?;
        el_rob.x += 1;
        el_rob.y = LesRobot.next().ok_or(anyhow!("pb y"))?.parse::<i32>()?;
        let en_str = LesRobot.next().ok_or(anyhow!("pb ori"))?;
        el_rob.set_orientation(en_str);
        i += 1;
        ///quand on le split il y a des "" qui reste, les lignes suivante permet de ne pas les compter
        while let Some(Letter) = LesInstrs.next() {
            if Letter == "" {
                continue;
            } else {
                el_rob.instr_list.push(Letter.to_string());
            }
        }
        engare.push(el_rob);
    }

    ///le monde
    let mut the_world = Monde {
        x: x,
        y: y,
        map: Vec::new(),
    };
    ///création, set des robots et display leur position de départ
    the_world.create_world();
    for xrob in 0..engare.len() {
        the_world.set_robot_position(&engare[xrob]);
    }
    the_world.display_world(&engare);
    ///éxecute les instruction de chaque robots et display le final
    for xrob in 0..engare.len() {
        engare[xrob].make_move(&mut the_world);
    }
    the_world.display_world(&engare);

    anyhow::Result::Ok(()) // like EXIT_SUCCESS somehow.
}


///les test vérifiant le bon fonctionnement
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_instructionR() {
        let el_char="R".to_string();
        assert!(matches!(read_char(&el_char), Instruction::Right))
    }

    #[test]
    fn test_Arrow_Orientation() {
        let fleche = String::from("↑ ");
        assert!(matches!(Arrow_Orientation(&Orientation::N), fleche))
    }


    #[test]
    fn test_display() {
        let mut test_engare: Vec<Robot> = Vec::new();
        let mut terre = Monde {
            x: 4,
            y: 4,
            map: Vec::new(),
        };
        terre.create_world();
        terre.display_world(&test_engare);
    }

    #[test]
    fn turn_test_and_set_ori() {
        let mut test_engare: Vec<Robot> = Vec::new();
        let mut proto_rob = Robot {
            id:0,
            x:3,
            y:3,
            ori:Orientation::N,
            instr_list: Vec::new(),
        };
        proto_rob.set_orientation("S");
        assert!(matches!(proto_rob.ori,Orientation::S));

        proto_rob.turn(Instruction::Left);
        assert!(matches!(proto_rob.ori,Orientation::E));
    }
}
